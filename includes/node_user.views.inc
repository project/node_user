<?php

/**
 * @file
 * Node User - Views integration
 */

/**
 * Implements hook_views_data_alter().
 */
function node_user_views_data() {
  $data['node_user']['table']['group'] = t('Node User');
  $data['node_user']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  $data['node_user']['nid'] = array(
    'title' => t('Node User - Node'),
    'help' => t("Create a relationship to the node that owns a user."),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'base field' => 'nid',
      'label' => t('node'),
    ),
  );
  $data['node_user']['uid'] = array(
    'title' => t('Node User - User'),
    'help' => t("Create a relationship to the node's proxy user."),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'label' => t('user'),
    ),
  );

  return $data;
}
