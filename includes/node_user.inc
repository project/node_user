<?php

/**
 * @file
 * Node User - Main include file
 */

/**
 * Determine whether a given node type is a node_user_type.
 *
 * @param string $type
 *   The node type to check.
 *
 * @return bool
 *   True if node_user is enabled on the given node_type.
 */
function node_user_is_node_user_type($type) {
  return variable_get('node_user_enable_' . $type, FALSE);
}

/**
 * Get the node's user uid.
 *
 * @param int $nid
 *   The node id of the node.
 *
 * @return int
 *   The user id of the node_user.
 */
function node_user_node_uid($nid) {
  return db_result(db_query('SELECT uid FROM {node_user} WHERE nid = %d', $nid));
}

/**
 * Get the user's node nid.
 *
 * @param int $uid
 *   The user id of the node_user.
 *
 * @return int
 *   The node id of the associated node.
 */
function node_user_user_nid($uid) {
  return db_result(db_query('SELECT nid FROM {node_user} WHERE uid = %d', $uid));
}

/**
 * A user_save wrapper function that skips notifications.
 *
 * @param object $account
 *   The user object for to modify or add.
 * @param array $array
 *   (optional) An array of fields and values to save.
 * @param string $category
 *   (optional) The category for storing profile information in.
 *
 * @return object
 *   A fully-loaded $user object upon successful save or FALSE on fail.
 */
function node_user_user_save($account, $array = array(), $category = 'account') {
  // user_save will send out notifications if configured
  // We don't want to send notifications, so disable the notifications
  // and set them back after.
  $status_activated = variable_get('user_mail_status_activated_notify', TRUE);
  $status_blocked = variable_get('user_mail_status_blocked_notify', TRUE);
  $GLOBALS['conf']['user_mail_status_activated_notify'] = FALSE;
  $GLOBALS['conf']['user_mail_status_blocked_notify'] = FALSE;
  $account = user_save($account, $array, $category);
  $GLOBALS['conf']['user_mail_status_activated_notify'] = $status_activated;
  $GLOBALS['conf']['user_mail_status_blocked_notify'] = $status_blocked;
  return $account;
}

/**
 * A user_delete wrapper function.
 *
 * Skips notifications and won't ever delete user 0 or 1.
 *
 * @param array $edit
 *   An array of submitted form values.
 * @param int $uid
 *   The user ID of the user to delete.
 */
function node_user_user_delete($edit, $uid) {
  // Never delete user 0 or 1.
  if (in_array($uid, array(0, 1))) {
    return;
  }

  // User_delete will send out a notification if configured
  // We don't want to send a notification, so disable the notification
  // and set it back after.
  $notify = variable_get('user_mail_status_deleted_notify', FALSE);
  $GLOBALS['conf']['user_mail_status_deleted_notify'] = FALSE;
  user_delete($edit, $uid);
  $GLOBALS['conf']['user_mail_status_deleted_notify'] = $notify;
}

/**
 * Node User Cleanup.
 *
 * Deletes node_user users and config variables by type.
 * It will be used on uninstall, and when nodetypes are deleted.
 *
 * @param string|array $types
 *   Either 'all' to cleanup all node types, or an array listing
 *   the nodetypes to cleanup. For convinience, we allow single
 *   node types to be passed as a plain string.
 * @param string|array $cleanup
 *   (optional) Either 'all' to cleanup all cleanup items, or an
 *   array of cleanup_items to perform. Defaults to 'all'.
 *   Possible values:
 *   - 'users': Cleanup node users .
 *   - 'variables': Cleanup config variables.
 */
function node_user_cleanup($types = array(), $cleanup = NULL) {
  // Determine what to cleanup.
  $cleanup_items = array('users', 'variables');
  $cleanup = (empty($cleanup) || $cleanup == 'all') ? $cleanup_items : $cleanup;

  // 'all' means all types.
  if ($types == 'all') {
    $types = array_keys(node_get_types());
    $user_query = 'SELECT uid FROM {node_user}';
    $user_args  = array();
  }
  else {
    // Allow single typename to be passed without wrapping array.
    $types = is_string($types) ? array($types) : $types;
    $user_query = 'SELECT nu.uid FROM {node_user} nu INNER JOIN {node} n ON n.nid = nu.nid WHERE n.type IN (' . db_placeholders($types, 'varchar') . ')';
    $user_args  = $types;
  }

  // Cleanup Users.
  if (in_array('users', $cleanup)) {
    // Delete users.
    $result = db_query($user_query, $user_args);
    while ($uid = db_result($result)) {
      // Note: we set the node_user_keep_node flag so we leave nodes intact.
      node_user_user_delete(array('node_user_keep_node' => TRUE), $uid);
    }
  }

  // Cleanup Variables.
  if (in_array('variables', $cleanup)) {
    // Build a list of variables to delete.
    $vars = array();
    foreach ($types as $type) {
      $vars += _node_user_node_type_vars($type);
    }
    // Delete variables.
    foreach ($vars as $var) {
      variable_del($var);
    }
  }
}

/**
 * Identify nodes that should have a user but don't yet.
 *
 * @param bool $count
 *   TRUE for a count of items. FALSE for a list if nids.
 * @param string $type
 *   (optional) The node type to check (defaults to all types).
 *
 * @return int|array
 *   Either a count, or an array of nids depending on $count.
 */
function node_user_missing_user($count = FALSE, $type = FALSE) {
  $args = array();
  $fields = $count ? 'COUNT(n.nid)' : 'n.nid';
  $where  = 'nu.uid IS NULL';
  if ($type) {
    $where .= " AND n.type = '%s'";
    $args[] = $type;
  }

  $query = 'SELECT ' . $fields . ' FROM {node} n LEFT JOIN {node_user} nu ON nu.nid = n.nid WHERE ' . $where;
  $result = db_query($query, $args);

  if ($count) {
    return db_result($result);
  }
  else {
    while ($item = db_result($result)) {
      $list[] = $item;
    }
    array_pop($list);
    return $list;
  }
}

/**
 * Create a new user for a node.
 *
 * @param object $node
 *   The node object for which to create a user.
 *
 * @return object
 *   The saved user object.
 */
function node_user_create_user($node) {
  $edit = array(
    'name' => _node_user_username($node),
    'mail' => _node_user_email($node),
    'init' => _node_user_email($node),
    'pass' => user_password(),
    'created' => $node->created,
    'status' => $node->status,
    'roles' => variable_get('node_user_roles_' . $node->type, array()),
  );

  // Allow modules to alter the user values before saving.
  drupal_alter('node_user_user', $edit, 'insert', $node);
  $account = node_user_user_save('', $edit);

  return $account;
}

/**
 * Generate Username.
 *
 * @param object $node
 *   The node object for which to create a username.
 *
 * @return string
 *   A unique username.
 */
function _node_user_username($node) {
  $name = variable_get('node_user_name_pattern_' . $type, '[title-raw]');
  $name = token_replace_multiple($name, array('global' => NULL, 'node' => $node));

  // Remove possible illegal characters.
  $name = preg_replace('/[^A-Za-z0-9_.-]/', '', $name);

  // Trim that value for spaces and length.
  $name = trim(substr($name, 0, USERNAME_MAX_LENGTH - 4));

  // Make sure we don't hand out a duplicate username.
  while (db_result(db_query("SELECT COUNT(uid) FROM {users} WHERE LOWER(name) = LOWER('%s')", $name)) > 0) {
    // If the username got too long, trim it back down.
    if (strlen($name) == USERNAME_MAX_LENGTH) {
      $name = substr($name, 0, USERNAME_MAX_LENGTH - 4);
    }

    // Append a random integer to the name.
    $name .= rand(0, 9);
  }

  return $name;
}

/**
 * Generate email address for a node-user.
 *
 * Address will be based on the node-author's email
 * and tagged or subaddressed ( RFC 5233 ) using the node-title
 * to ensure uniqueness.
 * eg. name@domain.tld becomes name+node_title@domain.tld
 * The +node_title part will be ignored by most mail servers and
 * the mail will be delivered to the node's author.
 * @see http://tools.ietf.org/html/rfc5233
 *
 * @param object $node
 *   The node object for which to create an email.
 *
 * @return string
 *   A unique email.
 */
function _node_user_email($node) {
  if ($node->uid) {
    $author = user_load($node->uid);
    $mail = $author->mail;
  }
  else {
    $mail = _node_user_anon_email($node->type);
  }

  list($name, $domain) = explode('@', $mail);

  // Grab the title and mangle it into an acceptable email address tag.
  $tag = preg_replace('/[^a-zA-Z0-9]+/', '_', trim(strtolower($node->title)));

  // Make sure the tag will fit in the max email size.
  $base_length = strlen($mail);
  $tag_max_length = EMAIL_MAX_LENGTH - $base_length - 1;
  $tag = substr($tag, 0, $tag_max_length);
  if (strlen($tag) < 1) {
    // If the email address is long enough that we can't fit even
    // a single character tag, then our only option is to stick
    // with the author's email. This will cause duplicate email
    // validation issues on the user edit form.
    return $mail;
  }

  // Email in form name+tag@domain.
  $email = $name . '+' . $tag . '@' . $domain;

  // Make sure we don't hand out a duplicate email.
  while (db_result(db_query("SELECT COUNT(uid) FROM {users} WHERE LOWER(mail) = LOWER('%s')", $email)) > 0) {
    $email = $name . '+' . $tag . '@' . $domain;

    // If the email got too long, trim the tag back down.
    if (strlen($name . '+' . $tag) == EMAIL_MAX_LENGTH) {
      $trim_len = min(4, strlen($tag));
      $tag = substr($tag, 0, $tag_max_length - $trim_len);
    }

    // Append a random integer to the tag piece.
    $tag .= rand(0, 9);
  }

  return $email;
}

/**
 * List node_type variables.
 *
 * @param string $type
 *   The node type to check.
 *
 * @return array
 *   An array of config variable names for this type.
 */
function _node_user_node_type_vars($type) {
  $vars[] = 'node_user_enable_' . $type;
  $vars[] = 'node_user_name_pattern_' . $type;
  $vars[] = 'node_user_anon_email_' . $type;
  $vars[] = 'node_user_roles_' . $type;
  return $vars;
}

/**
 * Get a default email address for a given type.
 *
 * @param string $type
 *   The node type to check.
 *
 * @return string
 *   A default email address to use for nodes of this type
 */
function _node_user_anon_email($type) {
  return variable_get('node_user_anon_email_' . $type, variable_get('site_mail', ini_get('sendmail_from')));
}
