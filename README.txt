
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Dependencies
 * Installation
 * Usage


INTRODUCTION
------------

Current Maintainer: Dylan Riordan <dylan.riordan@gmail.com>

Node User is intended to allow nodes to act as users. It does this by creating
a user for each node. This functionality can be configured by node type.

This module differs from Content Profile. Content profile is designed to extend
a user profile with content from a node. Node User on the other hand is intended
to extend a node with a user. The node can still be authored by a different
user, and indeed, this is kind of the point.

The practical example for this module would be Facebook "Pages". A Facebook
"Page" is not the same thing as a user, but it can ACT as a user, making
posts, commenting and voting on things, etc.

Node user lays the foundation of maintaining the node-user relationship. It
also provides views relationships linking the users and nodes.

Future developments may include:
  * A switch user interface to allow users to "Act As" one of their node users.
  * Tighter integration with core and common contrib modules
    - Comments
    - Voting API
    - User Points
    - < insert your sugestion here >

What about Drupal 7?
My current thinking for Drupal 7 is that this should be rewritten as a field
that can be used on any entity. In this case the would be called Entity User


DEPENDENCIES
------------

 * node
 * user
 * token


INSTALLATION
------------

Installing the Node User module is simple:

1) Copy the node_user folder to the modules folder in your installation.

2) Enable the module using Administer -> Modules (/admin/build/modules)

3) Enable and configure Node User for the appropriate node types. The
   settings are on the node-type form at (/admin/content/node-type/<type-name>)
   - "Allow nodes of this type to act as a user" - turns it on
   - If you have existing content of this type, you will see an option
     to retroactively create the missing users.
   - The username can be configured using tokens. Note that random numbers will
     be appended if necessary to ensure a unique username
   - The default roles can also be configured.


USAGE
------------

Aside from managing the node/user relationship this module doesn't do much at
this point. For Views, you may make use of the provided relationships to pull
in the user or node informaiton as needed.
